/*
 * Copyright (c) 2017, Expert Soft Inc. <expert-soft.by/>
 *
 * All portions of the code written by Expert Soft Inc. are property of Expert Soft Inc. 
 * All rights reserved.
 * 
 * Expert Soft Inc.
 * Minsk, Belarus
 * Web: expert-soft.by
 */
package by.es.calculator;

/**
 * Calculator.
 *
 * @author Maksim Piatrou <maksim.piatrou@expert-soft.by>
 * @package by.es.calculator
 * @link http://expert-soft.by/
 * @copyright 2017 Expert Soft Inc.
 */
public class Calculator
{
	public Node evaluate(String expression)
	{
		return parse(expression);
	}

	private Node parse(String expression)
	{
		String[] operandsAdd = expression.split("[\\+]");

		if (operandsAdd.length > 1)
		{
			String lastOperand = operandsAdd[operandsAdd.length - 1];
			Node nodeLeft = parse(expression.substring(0, expression.length() - lastOperand.length() - 1));
			Node nodeRight = parse(lastOperand);
			return new Node(nodeLeft, nodeRight, '+', 0);
		}

		String[] operandsSub = expression.split("[\\-]");
		if (operandsSub.length > 1)
		{
			String lastOperand = operandsSub[operandsSub.length - 1];
			Node nodeLeft = parse(expression.substring(0, expression.length() - lastOperand.length() - 1));
			Node nodeRight = parse(lastOperand);
			return new Node(nodeLeft, nodeRight, '-', 0);
		}

		String[] operandsMult = expression.split("[\\*]");
		if (operandsMult.length > 1)
		{
			String lastOperand = operandsMult[operandsMult.length - 1];
			Node nodeLeft = parse(expression.substring(0, expression.length() - lastOperand.length() - 1));
			Node nodeRight = parse(lastOperand);
			return new Node(nodeLeft, nodeRight, '*', 0);
		}

		String[] operandsDiv = expression.split("[\\/]");
		if (operandsDiv.length > 1)
		{
			String lastOperand = operandsDiv[operandsDiv.length - 1];
			Node nodeLeft = parse(expression.substring(0, expression.length() - lastOperand.length() - 1));
			Node nodeRight = parse(lastOperand);
			return new Node(nodeLeft, nodeRight, '/', 0);
		}

		return new Node(null, null, '#', Double.valueOf(expression));
	}
}
