The Problem

Consider a simple arithmetical expression with +-\*/ operators (no parenthesis).  e.g 1+2\*3 - 4/2

Requirement 1. Design a structure to represent the expression as a tree with the leaf nodes being the values and connection nodes being the operations. Each node must be able to evaluate and print itself. 

Requirement 2. Create a calculator class that accepts a string expression, parses and creates a tree of nodes according to the first requirement. Having the tree root node it would be easy to calculate expression value as this is already implemented in requirement 1.
The candidate is presented with the initial workable solution of the problem.
